import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { Request } from 'express';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {

    constructor(private _authService: AuthService) { }

    @Post('signin')
    async SignInAsync(@Body() body: any, req: Request) {
        const { sessionId, alias } = body
        return this._authService.signInAsync(sessionId, alias)
    }
}
