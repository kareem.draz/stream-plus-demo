import { Injectable, Post } from '@nestjs/common';
import { HttpModule, HttpService } from '@nestjs/axios';
import { Observable, catchError } from 'rxjs';
import { AxiosResponse } from 'axios';
import axios from 'axios'

@Injectable()
export class AuthService {

    constructor(private _http: HttpService) { }

    private BC_API_URL = process.env.BC_API_URL

    async signInAsync(sessionId: string, alias: string) {
        const payload = { // Ideally would follow interface/class 
            SchemaName: "disney",
            SessionId: sessionId,
            Alias: alias,
            AgentId: null,
            IPAddress: "70.26.153.78",
            BrowserFingerprint: "fe69257a4b5b3164f1b3a1043114725b76581c38d72d111c3894fba609533df5",
            Parameters: null
        }
        console.log(this.BC_API_URL + '/bc/agency/vc/auth/signin')
        // const BCResponse = this._http.post(this.BC_API_URL + '/bc/agency/vc/auth/signin', payload) -> Manage Observables
        const BCResponse = await axios.post(this.BC_API_URL + '/bc/agency/vc/auth/signin', payload, {
            headers: {
                'Hash': 'jhKbcsQeRUrJqY2ey9ohXVDF85odKHJT'
            }
        })
        console.log(BCResponse.data)
        return BCResponse
    }

}
